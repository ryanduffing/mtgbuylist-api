﻿// -------------------------------------------------------------------------------
// <copyright file="SetService.cs" company="Ryan Duffing">
//   Copyright (c) 2015 All Rights Reserved
//   <author>Ryan Duffing</author>
// </copyright>
// -------------------------------------------------------------------------------

namespace MTGBuyList.Service.Services
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using AutoMapper.QueryableExtensions;
    using MTGBuyList.Data.Entities;
    using MTGBuyList.Data.Repositories.Interfaces;
    using MTGBuyList.Service.Services.Interfaces;
    using Newtonsoft.Json;

    /// <summary>
    /// The set service
    /// </summary>
    /// <seealso cref="MTGBuyList.Service.Services.Interfaces.ISetService" />
    public class SetService : ISetService
    {
        #region Fields
        /// <summary>
        /// The set repository
        /// </summary>
        private readonly ISetRepository setRepository;
        #endregion

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="SetService"/> class.
        /// </summary>
        /// <param name="setRepository">The set repository.</param>
        public SetService(ISetRepository setRepository)
        {
            this.setRepository = setRepository;
        }
        #endregion

        #region Methods

        public IQueryable<Core.DTOs.Set> GetSetById(int id)
        {
            return this.setRepository.Get(id).Project().To<Core.DTOs.Set>();
        }

        /// <summary>
        /// Gets the collection of sets.
        /// </summary>
        /// <returns>The collection of sets</returns>
        public IQueryable<Core.DTOs.Set> GetSets()
        {
            return this.setRepository.GetAll().Project().To<Core.DTOs.Set>();
        }

        /// <summary>
        /// Removes all sets from the data repository.
        /// </summary>
        public void RemoveAllSets()
        {
            this.setRepository.DeleteAllSets();
            this.setRepository.Save();
        }

        /// <summary>
        /// Updates the all the sets in the data repository.
        /// </summary>
        public void UpdateAllSets()
        {
            var setList = new List<Set>();

            var jsonSets = new WebClient().DownloadString("https://api.deckbrew.com/mtg/sets");
            dynamic sets = JsonConvert.DeserializeObject(jsonSets);

            foreach (var set in sets)
            {
                string setName = set.name;

                setList.Add(
                    new Set
                    {
                        Abbreviation = set.id,
                        Name = setName.Replace("â€”", "-")
                    });
            }

            this.setRepository.AddMany(setList);
            this.setRepository.Save();
        }
        #endregion
    }
}
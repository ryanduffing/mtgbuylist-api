﻿// -------------------------------------------------------------------------------
// <copyright file="WebApiConfig.cs" company="Ryan Duffing">
//   Copyright (c) 2016 All Rights Reserved
//   <author>Duffing, Ryan</author>
// </copyright>
// -------------------------------------------------------------------------------

namespace MTGBuyList.Web
{
    using System.Linq;
    using System.Net.Http.Formatting;
    using System.Web.Http;
    using MTGBuyList.Web.Formatters;
    using Newtonsoft.Json.Serialization;
    using WebGrease.Css.Extensions;

    /// <summary>
    ///  Configures the web api routes.
    /// </summary>
    public static class WebApiConfig
    {
        /// <summary>
        ///  Register web api configuration settings.
        /// </summary>
        /// <param name="config">Web api configuration object.</param>
        #region Methods
        public static void Register(HttpConfiguration config)
        {
            // Web API routes
            ////config.MapHttpAttributeRoutes();

            // Configure formatters
            config.Formatters.Add(new BrowserJsonFormatter());

            // Set camel case property names for all JSON formatters
            config.Formatters.OfType<JsonMediaTypeFormatter>().ForEach(f => f.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver());

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new
                {
                    id = RouteParameter.Optional
                });
        }
        #endregion
    }
}
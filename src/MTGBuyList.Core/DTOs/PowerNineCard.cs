﻿// -------------------------------------------------------------------------------
// <copyright file="PowerNineCard.cs" company="Ryan Duffing">
//   Copyright (c) 2015 All Rights Reserved
//   <author>Ryan Duffing</author>
// </copyright>
// -------------------------------------------------------------------------------

namespace MTGBuyList.Core.DTOs
{
    using System;

    /// <summary>
    /// The PowerNine card price data transfer object.
    /// </summary>
    public class PowerNineCard
    {
        #region Properties
        /// <summary>Gets or sets the PowerNine record identifier.</summary>
        /// <value>The PowerNine record identifier.</value>
        public int? PowerNineRecordId { get; set; }

        /// <summary>Gets or sets the card identifier.</summary>
        /// <value>The card identifier.</value>
        public int? CardId { get; set; }

        /// <summary>Gets or sets the buy price.</summary>
        /// <value>The buy price.</value>
        public double BuyPrice { get; set; }

        /// <summary>Gets or sets the PowerNine card name.</summary>
        /// <value>The PowerNine card name.</value>
        public string PowerNineName { get; set; }

        /// <summary>Gets or sets a value indicating whether the price is for the foil version of the card.</summary>
        /// <value>A value indicating whether the price is for the foil version of the card.</value>
        public bool IsFoil { get; set; }

        /// <summary>Gets or sets the full name.</summary>
        /// <value>The full name.</value>
        public string FullName { get; set; }

        /// <summary>Gets or sets the name as ASCII characters.</summary>
        /// <value>The name as ASCII characters.</value>
        public string ASCIIName { get; set; }

        /// <summary>Gets or sets the name of the color.</summary>
        /// <value>The name of the color.</value>
        public string ColorName { get; set; }

        /// <summary>Gets or sets the identifier of the set.</summary>
        /// <value>The identifier of the set.</value>
        public int? SetId { get; set; }

        /// <summary>Gets or sets the name of the set.</summary>
        /// <value>The name of the set.</value>
        public string SetName { get; set; }

        /// <summary>
        ///  Gets or sets the rarity name for the card.
        /// </summary>
        public string RarityName { get; set; }

        /// <summary>
        ///  Gets or sets the rarity id for the card.
        /// </summary>
        public int? RarityId { get; set; }

        /// <summary>Gets or sets the release date.</summary>
        /// <value>The release date.</value>
        public DateTime? ReleaseDate { get; set; }

        /// <summary>Gets or sets the image URL.</summary>
        /// <value>The image URL.</value>
        public string ImageUrl { get; set; }

        /// <summary>Gets or sets the multiverse identifier.</summary>
        /// <value>The multiverse identifier.</value>
        public long? MultiverseId { get; set; }
        #endregion
    }
}
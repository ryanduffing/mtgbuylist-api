// -------------------------------------------------------------------------------
// <copyright file="IPowerNineService.cs" company="Ryan Duffing">
//   Copyright (c) 2015 All Rights Reserved
//   <author>Ryan Duffing</author>
// </copyright>
// -------------------------------------------------------------------------------

namespace MTGBuyList.Service.Services.Interfaces
{
    /// <summary>
    /// The PowerNine Service interface for updating card prices and other information.
    /// </summary>
    public interface IPowerNineService
    {
        #region Methods
        /// <summary>Removes all PowerNine records.</summary>
        void RemovePowerNineRecords();

        /// <summary>Updates the PowerNine records using the specified file URI.</summary>
        /// <param name="fileURI">The PowerNine card buy list file URI.</param>
        void UpdatePowerNineRecords(string fileURI);
        #endregion
    }
}
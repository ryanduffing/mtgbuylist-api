﻿// -------------------------------------------------------------------------------
// <copyright file="CardService.cs" company="Ryan Duffing">
//   Copyright (c) 2015 All Rights Reserved
//   <author>Ryan Duffing</author>
// </copyright>
// -------------------------------------------------------------------------------

namespace MTGBuyList.Service.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using AutoMapper.QueryableExtensions;
    using MTGBuyList.Data.Entities;
    using MTGBuyList.Data.Repositories.Interfaces;
    using MTGBuyList.Service.Services.Interfaces;
    using Newtonsoft.Json.Linq;
    using UnidecodeSharpFork;

    /// <summary>The card service.</summary>
    public class CardService : ICardService
    {
        #region Fields
        /// <summary>The card repository for accessing data</summary>
        private readonly ICardRepository cardRepository;

        /// <summary>The set repository</summary>
        private readonly ISetRepository setRepository;

        /// <summary>The PowerNine record repository</summary>
        private readonly IPowerNineRecordRepository powerNineRecordRepository;

        /// <summary>The text file reader</summary>
        private readonly ITextFileReader textFileReader;

        /// <summary>The color service</summary>
        private readonly IColorService colorService;

        /// <summary>
        ///  The rarity service.
        /// </summary>
        private readonly IRarityService rarityService;
        #endregion

        #region Constructors
        /// <summary>Initializes a new instance of the <see cref="CardService"/> class.</summary>
        /// <param name="cardRepository">The card repository.</param>
        /// <param name="setRepository">The set repository.</param>
        /// <param name="powerNineRecordRepository">The PowerNine record repository.</param>
        /// <param name="textFileReader">The text file reader.</param>
        /// <param name="colorService">The color service.</param>
        /// <param name="rarityService">The rarity service.</param>
        public CardService(
            ICardRepository cardRepository,
            ISetRepository setRepository,
            IPowerNineRecordRepository powerNineRecordRepository,
            ITextFileReader textFileReader,
            IColorService colorService,
            IRarityService rarityService)
        {
            this.cardRepository = cardRepository;
            this.setRepository = setRepository;
            this.powerNineRecordRepository = powerNineRecordRepository;
            this.textFileReader = textFileReader;
            this.colorService = colorService;
            this.rarityService = rarityService;
        }
        #endregion

        #region Methods
        /// <summary>Gets the cards.</summary>
        /// <returns>A queryable source of cards</returns>
        public IQueryable<Core.DTOs.Card> GetCards()
        {
            return this.cardRepository.GetAll().Project().To<Core.DTOs.Card>();
        }

        /// <summary>Gets the PowerNine cards.</summary>
        /// <returns>A queryable source of PowerNine cards</returns>
        public IQueryable<Core.DTOs.PowerNineCard> GetPowerNineCards()
        {
            return this.powerNineRecordRepository.GetAll().Project().To<Core.DTOs.PowerNineCard>();
        }

        /// <summary>Removes all cards.</summary>
        public void RemoveAllCards()
        {
            this.cardRepository.DeleteAllCards();
            this.cardRepository.Save();
        }

        /// <summary>Updates the cards and sets using the specified file URI.</summary>
        /// <param name="fileURI">The card list file URI.</param>
        public void UpdateCardsFromFile(string fileURI)
        {
            var setList = new List<Set>();
            var cardList = new List<Card>();
            var colorList = this.colorService.GetColors().ToList();
            var rarityList = this.rarityService.GetRarities().ToList();

            var jsonCardsString = this.textFileReader.ReadFile(fileURI);

            ////dynamic cardSets = JsonConvert.DeserializeObject(jsonCardsString);
            var cardSets = JObject.Parse(jsonCardsString);

            // Loop through the card sets which contain cards
            foreach (var cardSet in cardSets)
            {
                var cardSetValue = cardSet.Value;

                // Create the set
                var set = new Set()
                {
                    Name = (string)cardSetValue["name"],
                    Abbreviation = (string)cardSetValue["code"],
                    ReleaseDate = cardSetValue["releaseDate"] != null ? DateTime.Parse((string)cardSetValue["releaseDate"]) : DateTime.Now
                    ////Name = cardSetValue.name.Value,
                    ////Abbreviation = cardSetValue.code.Value,
                    ////ReleaseDate = DateTime.Parse(cardSetValue.releaseDate.Value)
                };

                setList.Add(set);

                // Loop through the set's cards
                foreach (var setCard in cardSetValue["cards"])
                {
                    var cardName = (string)setCard["name"];
                    var multiverseId = setCard["multiverseid"] != null ? (long)setCard["multiverseid"] : 0;
                    var mciNumber = (string)setCard["mciNumber"];
                    var number = (string)setCard["number"];
                    var rarity = (string)setCard["rarity"];
                    var colorString = this.colorService.GetCardColor(setCard["colors"]?.ToObject<string[]>(), setCard["types"]?.ToObject<string[]>());

                    ////var cardName = (string)setCard.name.Value;
                    ////var multiverseId = setCard.multiverseid != null ? setCard.multiverseid.Value : 0;
                    ////var colorString = (string)this.colorService.GetCardColor(setCard.colors, setCard.types);
                    var colorId = colorList.FirstOrDefault(c => string.Equals(c.Name, colorString, StringComparison.CurrentCultureIgnoreCase))?.ColorId;
                    var rarityId = rarityList.FirstOrDefault(x => string.Equals(x.Name, rarity, StringComparison.CurrentCultureIgnoreCase))?.RarityId;

                    // Create the card
                    var card = new Card()
                    {
                        FullName = cardName,
                        ASCIIName = cardName.Unidecode(),
                        Set = set,
                        ColorId = colorId,
                        RarityId = rarityId,
                        ImageUrl = $@"https://image.deckbrew.com/mtg/multiverseid/{multiverseId}.jpg",
                        MultiverseId = multiverseId,
                        MciNumber = mciNumber,
                        Number = number
                    };

                    cardList.Add(card);
                }
            }

            // Save the Sets
            this.setRepository.AddMany(setList);
            this.setRepository.Save();

            // Save the Cards
            this.cardRepository.AddMany(cardList);
            this.cardRepository.Save();
        }
        #endregion
    }
}
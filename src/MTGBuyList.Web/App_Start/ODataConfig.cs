﻿// -------------------------------------------------------------------------------
// <copyright file="ODataConfig.cs" company="Ryan Duffing">
//   Copyright (c) 2016 All Rights Reserved
//   <author>Duffing, Ryan</author>
// </copyright>
// -------------------------------------------------------------------------------

namespace MTGBuyList.Web
{
    using System.Web.Http;
    using System.Web.Http.Cors;
    using System.Web.OData.Builder;
    using System.Web.OData.Extensions;
    using Microsoft.OData.Edm;
    using MTGBuyList.Core.DTOs;

    /// <summary>
    ///  Configures the odata routes and models.
    /// </summary>
    public static class ODataConfig
    {
        #region Methods
        /// <summary>
        /// Registers the OData with the specified configuration.
        /// </summary>
        /// <param name="config">The configuration.</param>
        public static void Register(HttpConfiguration config)
        {
            // Configure OData routing
            config.MapHttpAttributeRoutes();

            config.MapODataServiceRoute(routeName: "ODataRoute", routePrefix: "api", model: GenerateEdmModel());

            // Setup CORS
            var origins = System.Configuration.ConfigurationManager.AppSettings.Get("Cors:AllowOrigins") ?? "*";
            var cors = new EnableCorsAttribute(origins, "*", "*");
            config.EnableCors(cors);
        }

        /// <summary>
        /// Generates the EDM model.
        /// </summary>
        /// <returns>An EDM model.</returns>
        private static IEdmModel GenerateEdmModel()
        {
            var builder = new ODataConventionModelBuilder();
            builder.EntitySet<Card>("Card");
            builder.EntitySet<Color>("Color");
            builder.EntitySet<Rarity>("Rarity");
            builder.EntitySet<Set>("Set");
            builder.EntitySet<PowerNineCard>("PowerNineCard").EntityType.HasKey(r => r.PowerNineRecordId);

            return builder.GetEdmModel();
        }
        #endregion
    }
}
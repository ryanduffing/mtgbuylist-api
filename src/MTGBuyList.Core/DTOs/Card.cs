﻿// -------------------------------------------------------------------------------
// <copyright file="Card.cs" company="Ryan Duffing">
//   Copyright (c) 2015 All Rights Reserved
//   <author>Ryan Duffing</author>
// </copyright>
// -------------------------------------------------------------------------------

namespace MTGBuyList.Core.DTOs
{
    using System;

    /// <summary>
    /// The card data transfer object.
    /// </summary>
    public class Card
    {
        #region Properties
        /// <summary>Gets or sets the card identifier.</summary>
        /// <value>The card identifier.</value>
        public int CardId { get; set; }

        /// <summary>Gets or sets the full name.</summary>
        /// <value>The full name.</value>
        public string FullName { get; set; }

        /// <summary>Gets or sets the card name as all ASCII characters.</summary>
        /// <value>The card name as all ASCII characters.</value>
        public string ASCIIName { get; set; }

        /// <summary>Gets or sets the name of the color.</summary>
        /// <value>The name of the color.</value>
        public string ColorName { get; set; }

        /// <summary>
        ///  Gets or sets the rarity name for the card.
        /// </summary>
        public string RarityName { get; set; }

        /// <summary>
        ///  Gets or sets the rarity id for the card.
        /// </summary>
        public int? RarityId { get; set; }

        /// <summary>Gets or sets the name of the set.</summary>
        /// <value>The name of the set.</value>
        public string SetName { get; set; }

        /// <summary>Gets or sets the release date.</summary>
        /// <value>The release date.</value>
        public DateTime? ReleaseDate { get; set; }

        /// <summary>Gets or sets the image URL.</summary>
        /// <value>The image URL.</value>
        public string ImageUrl { get; set; }

        /// <summary>Gets or sets the multiverse identifier.</summary>
        /// <value>The multiverse identifier.</value>
        public long? MultiverseId { get; set; }

        /// <summary>Gets or sets the MCI number.</summary>
        /// <value>The MCI number.</value>
        public string MciNumber { get; set; }

        /// <summary>Gets or sets the number.</summary>
        /// <value>The number.</value>
        public string Number { get; set; }

        /// <summary>
        ///  Gets or sets the rarity.
        /// </summary>
        public Rarity Rarity { get; set; }

        /// <summary>Gets or sets the color.</summary>
        /// <value>The color.</value>
        public Color Color { get; set; }

        /// <summary>Gets or sets the set.</summary>
        /// <value>The set.</value>
        public Set Set { get; set; }
        #endregion

        ////public ICollection<PowerNineRecord> PowerNineRecords { get; set; }
    }
}
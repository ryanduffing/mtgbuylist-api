﻿// -------------------------------------------------------------------------------
// <copyright file="ColorConstants.cs" company="Ryan Duffing">
//   Copyright (c) 2016 All Rights Reserved
//   <author>Duffing, Ryan</author>
// </copyright>
// -------------------------------------------------------------------------------

namespace MTGBuyList.Core.Constants
{
    /// <summary>
    /// Class containing the Color constants.
    /// </summary>
    public static class ColorConstants
    {
        /// <summary>
        ///  The color white.
        /// </summary>
        public const string White = "white";

        /// <summary>
        ///  The color blue.
        /// </summary>
        public const string Blue = "blue";

        /// <summary>
        ///  The color black.
        /// </summary>
        public const string Black = "black";

        /// <summary>
        ///  The color red.
        /// </summary>
        public const string Red = "red";

        /// <summary>
        ///  The color green.
        /// </summary>
        public const string Green = "green";

        /// <summary>
        ///  The color gold.
        /// </summary>
        public const string Gold = "gold";

        /// <summary>
        ///  The color land.
        /// </summary>
        public const string Land = "land";

        /// <summary>
        ///  The color artifact.
        /// </summary>
        public const string Artifact = "artifact";

        /// <summary>
        ///  The color colorless.
        /// </summary>
        public const string Colorless = "colorless";
    }
}

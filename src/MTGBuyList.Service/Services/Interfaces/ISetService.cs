﻿// -------------------------------------------------------------------------------
// <copyright file="ISetService.cs" company="Ryan Duffing">
//   Copyright (c) 2015 All Rights Reserved
//   <author>Ryan Duffing</author>
// </copyright>
// -------------------------------------------------------------------------------

namespace MTGBuyList.Service.Services.Interfaces
{
    using System.Linq;

    /// <summary>
    /// The set service.
    /// </summary>
    public interface ISetService
    {
        IQueryable<Core.DTOs.Set> GetSetById(int id);

        /// <summary>
        /// Gets the collection of sets.
        /// </summary>
        /// <returns>The collection of sets</returns>
        IQueryable<Core.DTOs.Set> GetSets();

        /// <summary>
        /// Removes all sets from the data repository.
        /// </summary>
        void RemoveAllSets();

        /// <summary>
        /// Updates the all the sets in the data repository.
        /// </summary>
        void UpdateAllSets();
    }
}

﻿// -------------------------------------------------------------------------------
// <copyright file="SetRepository.cs" company="Ryan Duffing">
//   Copyright (c) 2015 All Rights Reserved
//   <author>Ryan Duffing</author>
// </copyright>
// -------------------------------------------------------------------------------

namespace MTGBuyList.Data.Repositories
{
    using System;
    using MTGBuyList.Data.Context;
    using MTGBuyList.Data.Entities;
    using MTGBuyList.Data.Repositories.Interfaces;

    /// <summary>
    /// The Set repository
    /// </summary>
    /// <seealso cref="MTGBuyList.Data.Repositories.Interfaces.ISetRepository" />
    public class SetRepository : GenericRepository<Set>, ISetRepository
    {
        #region Fields
        /// <summary>The database context</summary>
        private readonly IMTGBuyListContext context;
        #endregion

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="SetRepository"/> class.
        /// </summary>
        /// <param name="context">The context.</param>
        public SetRepository(IMTGBuyListContext context)
            : base(context)
        {
            this.context = context;
        }
        #endregion

        #region Methods
        /// <summary>Deletes all set entities.</summary>
        /// <returns>A value indicating whether the delete was successful.</returns>
        public bool DeleteAllSets()
        {
            try
            {
                this.context.Database.ExecuteSqlCommand("DELETE Sets");
                return true;
            }
            catch (Exception)
            {
                // TODO log this error
                return false;
            }
        }
        #endregion
    }
}
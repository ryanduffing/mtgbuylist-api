// -------------------------------------------------------------------------------
// <copyright file="NinjectWebCommon.cs" company="Ryan Duffing">
//   Copyright (c) 2015 All Rights Reserved
//   <author>Ryan Duffing</author>
// </copyright>
// -------------------------------------------------------------------------------

using MTGBuyList.Web;

[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(NinjectWebCommon), "Stop")]

namespace MTGBuyList.Web
{
    using System;
    using System.Web;
    using System.Web.Http;
    using Microsoft.Web.Infrastructure.DynamicModuleHelper;
    using MTGBuyList.Data.Context;
    using MTGBuyList.Data.Repositories;
    using MTGBuyList.Data.Repositories.Interfaces;
    using MTGBuyList.Service.Services;
    using MTGBuyList.Service.Services.Interfaces;
    using Ninject;
    using Ninject.Web.Common;
    using Ninject.Web.WebApi;

    /// <summary>
    /// The configuration for dependency injection using Ninject.
    /// </summary>
    public static class NinjectWebCommon
    {
        #region Fields
        /// <summary>
        ///  Bootstrapper object.
        /// </summary>
        private static readonly Bootstrapper Bootstrapper = new Bootstrapper();
        #endregion

        #region Methods
        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start()
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            Bootstrapper.Initialize(CreateKernel);
        }

        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            Bootstrapper.ShutDown();
        }

        /// <summary>
        /// Creates the kernel that will manage the application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

                RegisterServices(kernel);

                // Register Ninject as dependency resolver
                GlobalConfiguration.Configuration.DependencyResolver = new NinjectDependencyResolver(kernel);

                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Load modules and/or register services.
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            ////kernel.Bind<MTGBuyListContext>().To<MTGBuyListContext>().InRequestScope(); 
            kernel.Bind<IMTGBuyListContext>().To<MTGBuyListContext>().InRequestScope(); // Same instance within a request (most important single instance)
            kernel.Bind<ICardRepository>().To<CardRepository>().InRequestScope();
            kernel.Bind<IColorRepository>().To<ColorRepository>().InRequestScope();
            kernel.Bind<ISetRepository>().To<SetRepository>().InRequestScope();
            kernel.Bind<IRarityRepository>().To<RarityRepository>().InRequestScope();
            kernel.Bind<IPowerNineRecordRepository>().To<PowerNineRecordRepository>().InRequestScope();

            kernel.Bind<IWebFileReader>().To<WebFileReader>();
            kernel.Bind<ITextFileReader>().To<TextFileReader>();
            kernel.Bind<ICardService>().To<CardService>();
            kernel.Bind<IColorService>().To<ColorService>();
            kernel.Bind<ISetService>().To<SetService>();
            kernel.Bind<IRarityService>().To<RarityService>();
            kernel.Bind<IPowerNineService>().To<PowerNineService>();
        }
        #endregion
    }
}
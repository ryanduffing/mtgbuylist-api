﻿// -------------------------------------------------------------------------------
// <copyright file="AutoMapperConfig.cs" company="Ryan Duffing">
//   Copyright (c) 2015 All Rights Reserved
//   <author>Ryan Duffing</author>
// </copyright>
// -------------------------------------------------------------------------------

namespace MTGBuyList.Web
{
    /// <summary>
    /// The configuration for the mappings using AutoMapper
    /// </summary>
    public static class AutoMapperConfig
    {
        #region Methods
        /// <summary>
        /// Registers the mappings using AutoMapper.
        /// </summary>
        public static void RegisterMappings()
        {
            AutoMapper.Mapper.CreateMap<Data.Entities.Card, Core.DTOs.Card>()
                .ForMember(dest => dest.RarityName, opt => opt.MapFrom(src => src.Rarity.Name))
                .ForMember(dest => dest.RarityId, opt => opt.MapFrom(src => src.Rarity.RarityId))
                .ForMember(dest => dest.SetName, opt => opt.MapFrom(src => src.Set.Name))
                .ForMember(dest => dest.ColorName, opt => opt.MapFrom(src => src.Color.Name))
                .ForMember(dest => dest.ReleaseDate, opt => opt.MapFrom(src => src.Set.ReleaseDate));

            AutoMapper.Mapper.CreateMap<Data.Entities.Color, Core.DTOs.Color>();

            AutoMapper.Mapper.CreateMap<Data.Entities.Set, Core.DTOs.Set>();
            AutoMapper.Mapper.CreateMap<Core.DTOs.Set, Data.Entities.Set>();

            AutoMapper.Mapper.CreateMap<Data.Entities.Rarity, Core.DTOs.Rarity>();
            AutoMapper.Mapper.CreateMap<Core.DTOs.Rarity, Data.Entities.Rarity>();

            AutoMapper.Mapper.CreateMap<Data.Entities.PowerNineRecord, Core.DTOs.PowerNineRecord>();

            AutoMapper.Mapper.CreateMap<Data.Entities.PowerNineRecord, Core.DTOs.PowerNineCard>()
                .ForMember(dest => dest.FullName, opt => opt.MapFrom(src => src.Card.FullName))
                .ForMember(dest => dest.ASCIIName, opt => opt.MapFrom(src => src.Card.ASCIIName))
                .ForMember(dest => dest.ColorName, opt => opt.MapFrom(src => src.Card.Color.Name))
                .ForMember(dest => dest.RarityId, opt => opt.MapFrom(src => src.Card.Rarity.RarityId))
                .ForMember(dest => dest.RarityName, opt => opt.MapFrom(src => src.Card.Rarity.Name))
                .ForMember(dest => dest.SetId, opt => opt.MapFrom(src => src.Card.Set.SetId))
                .ForMember(dest => dest.SetName, opt => opt.MapFrom(src => src.Card.Set.Name))
                .ForMember(dest => dest.ReleaseDate, opt => opt.MapFrom(src => src.Card.Set.ReleaseDate))
                .ForMember(dest => dest.ImageUrl, opt => opt.MapFrom(src => src.Card.ImageUrl))
                .ForMember(dest => dest.MultiverseId, opt => opt.MapFrom(src => src.Card.MultiverseId));

            // .ForMember(dest => dest.AccountName, opt => opt.MapFrom(src => XssEscapeString(src.AccountName)))
        }
        #endregion
    }
}
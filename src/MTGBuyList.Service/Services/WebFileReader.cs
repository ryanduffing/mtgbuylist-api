﻿// -------------------------------------------------------------------------------
// <copyright file="WebFileReader.cs" company="Ryan Duffing">
//   Copyright (c) 2015 All Rights Reserved
//   <author>Ryan Duffing</author>
// </copyright>
// -------------------------------------------------------------------------------

namespace MTGBuyList.Service.Services
{
    using System.Net;
    using MTGBuyList.Service.Services.Interfaces;

    /// <summary>
    /// Reads a file from the web.
    /// </summary>
    public class WebFileReader : IWebFileReader
    {
        #region Methods
        /// <summary>Reads the web file.</summary>
        /// <param name="fileURI">The file URI.</param>
        /// <returns>A string containing the contents of the web file.</returns>
        public string ReadWebFile(string fileURI)
        {
            string html;
            using (var client = new WebClient())
            {
                html = client.DownloadString(fileURI);
            }

            return html;
        }
        #endregion
    }
}
﻿// -------------------------------------------------------------------------------
// <copyright file="Card.cs" company="Ryan Duffing">
//   Copyright (c) 2015 All Rights Reserved
//   <author>Ryan Duffing</author>
// </copyright>
// -------------------------------------------------------------------------------

namespace MTGBuyList.Data.Entities
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// The Card data entity.
    /// </summary>
    public class Card
    {
        #region Properties
        /// <summary>Gets or sets the card identifier.</summary>
        /// <value>The card identifier.</value>
        [Key]
        public int CardId { get; set; }

        /// <summary>Gets or sets the full card name.</summary>
        /// <value>The full card name.</value>
        public string FullName { get; set; }

        /// <summary>Gets or sets the ASCII card name.</summary>
        /// <value>The ASCII card name.</value>
        public string ASCIIName { get; set; }

        /// <summary>Gets or sets the set identifier.</summary>
        /// <value>The set identifier.</value>
        [ForeignKey("Set")]
        public int? SetId { get; set; }

        /// <summary>Gets or sets the color identifier.</summary>
        /// <value>The color identifier.</value>
        [ForeignKey("Color")]
        public int? ColorId { get; set; }

        /// <summary>
        ///  Gets or sets the rarity identifier.
        /// </summary>
        [ForeignKey("Rarity")]
        public int? RarityId { get; set; }

        /// <summary>Gets or sets the URL for the card image</summary>
        /// <value>The card image.</value>
        public string ImageUrl { get; set; }

        /// <summary>Gets or sets the multiverse identifier</summary>
        /// <value>The multiverse identifier.</value>
        public long? MultiverseId { get; set; }

        /// <summary>Gets or sets the MCI number (the card number in the set)</summary>
        /// <value>The MCI number (the card number in the set).</value>
        public string MciNumber { get; set; }

        /// <summary>Gets or sets the number (the card number in the set - [230a] format for lands)</summary>
        /// <value>The number (the card number in the set - [230a] format for lands).</value>
        public string Number { get; set; }

        /// <summary>
        ///  Gets or sets the rarity.
        /// </summary>
        public virtual Rarity Rarity { get; set; }

        /// <summary>Gets or sets the color.</summary>
        /// <value>The color.</value>
        public virtual Color Color { get; set; }

        /// <summary>Gets or sets the set.</summary>
        /// <value>The set.</value>
        public virtual Set Set { get; set; }

        /// <summary>Gets or sets the PowerNine collection.</summary>
        /// <value>The PowerNine record collection.</value>
        public virtual ICollection<PowerNineRecord> PowerNineRecords { get; set; }
        #endregion
    }
}
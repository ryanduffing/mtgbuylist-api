﻿// -------------------------------------------------------------------------------
// <copyright file="RarityService.cs" company="Ryan Duffing">
//   Copyright (c) 2015 All Rights Reserved
//   <author>Ryan Duffing</author>
// </copyright>
// -------------------------------------------------------------------------------

namespace MTGBuyList.Service.Services
{
    using System.Linq;
    using AutoMapper.QueryableExtensions;
    using MTGBuyList.Core.Constants;
    using MTGBuyList.Data.Entities;
    using MTGBuyList.Data.Repositories.Interfaces;
    using MTGBuyList.Service.Services.Interfaces;

    /// <summary>
    ///  The rarity service.
    /// </summary>
    public class RarityService : IRarityService
    {
        #region Fields
        /// <summary>
        /// The rarity repository
        /// </summary>
        private readonly IRarityRepository rarityRepository;
        #endregion

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="RarityService"/> class.
        /// </summary>
        /// <param name="rarityRepository">The rarity repository.</param>
        public RarityService(IRarityRepository rarityRepository)
        {
            this.rarityRepository = rarityRepository;
        }
        #endregion

        /// <summary>
        /// Gets the collection of rarity types.
        /// </summary>
        /// <returns>The collection of rarity types</returns>
        public IQueryable<Core.DTOs.Rarity> GetRarities()
        {
            return this.rarityRepository.GetAll().Project().To<Core.DTOs.Rarity>();
        }

        /// <summary>
        /// Removes all rarity types from the data repository.
        /// </summary>
        public void RemoveAllRarities()
        {
            this.rarityRepository.DeleteAllRarities();
            this.rarityRepository.Save();
        }

        /// <summary>
        /// Updates all the rarity types in the data repository.
        /// </summary>
        public void UpdateAllRarities()
        {
            var rarities = new string[]
            {
                RarityConstants.Common,
                RarityConstants.MythicRare,
                RarityConstants.Rare,
                RarityConstants.Timeshifted,
                RarityConstants.Uncommon,
                RarityConstants.BasicLand,
                RarityConstants.Special
            };

            var rarityList = rarities.Select(
                rarity => new Rarity
                {
                    Name = rarity
                }).ToList();

            this.rarityRepository.AddMany(rarityList);
            this.rarityRepository.Save();
        }
    }
}

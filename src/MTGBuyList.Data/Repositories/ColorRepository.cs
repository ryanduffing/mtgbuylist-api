﻿// -------------------------------------------------------------------------------
// <copyright file="ColorRepository.cs" company="Ryan Duffing">
//   Copyright (c) 2015 All Rights Reserved
//   <author>Ryan Duffing</author>
// </copyright>
// -------------------------------------------------------------------------------

namespace MTGBuyList.Data.Repositories
{
    using System;
    using MTGBuyList.Data.Context;
    using MTGBuyList.Data.Entities;
    using MTGBuyList.Data.Repositories.Interfaces;

    /// <summary>
    /// The Color repository
    /// </summary>
    /// <seealso cref="MTGBuyList.Data.Repositories.Interfaces.IColorRepository" />
    public class ColorRepository : GenericRepository<Color>, IColorRepository
    {
        #region Fields
        /// <summary>The database context</summary>
        private readonly IMTGBuyListContext context;
        #endregion

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="ColorRepository"/> class.
        /// </summary>
        /// <param name="context">The context.</param>
        public ColorRepository(IMTGBuyListContext context)
            : base(context)
        {
            this.context = context;
        }
        #endregion

        #region Methods
        /// <summary>Deletes all color entities.</summary>
        /// <returns>A value indicating whether the delete was successful.</returns>
        public bool DeleteAllColors()
        {
            try
            {
                this.context.Database.ExecuteSqlCommand("DELETE Colors");
                return true;
            }
            catch (Exception)
            {
                // TODO log this error
                return false;
            }
        }
        #endregion
    }
}
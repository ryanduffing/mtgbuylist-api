﻿// -------------------------------------------------------------------------------
// <copyright file="Set.cs" company="Ryan Duffing">
//   Copyright (c) 2015 All Rights Reserved
//   <author>Ryan Duffing</author>
// </copyright>
// -------------------------------------------------------------------------------

namespace MTGBuyList.Core.DTOs
{
    using System;

    /// <summary>
    /// The set data transfer object.
    /// </summary>
    public class Set
    {
        #region Properties
        /// <summary>Gets or sets the abbreviation.</summary>
        /// <value>The abbreviation.</value>
        public string Abbreviation { get; set; }

        /// <summary>Gets or sets the set identifier.</summary>
        /// <value>The set identifier.</value>
        public int SetId { get; set; }

        /// <summary>Gets or sets the image URL.</summary>
        /// <value>The image URL.</value>
        public string ImageUrl { get; set; }

        /// <summary>Gets or sets the name.</summary>
        /// <value>The name.</value>
        public string Name { get; set; }

        /// <summary>Gets or sets the release date.</summary>
        /// <value>The release date.</value>
        public DateTime? ReleaseDate { get; set; }
        #endregion
    }
}
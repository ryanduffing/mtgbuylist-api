// -------------------------------------------------------------------------------
// <copyright file="ICardService.cs" company="Ryan Duffing">
//   Copyright (c) 2015 All Rights Reserved
//   <author>Ryan Duffing</author>
// </copyright>
// -------------------------------------------------------------------------------

namespace MTGBuyList.Service.Services.Interfaces
{
    using System.Linq;

    /// <summary>
    ///  The card service interface.
    /// </summary>
    public interface ICardService
    {
        #region Methods
        /// <summary>Gets the cards.</summary>
        /// <returns>A queryable source of cards</returns>
        IQueryable<Core.DTOs.Card> GetCards();

        /// <summary>Gets the PowerNine cards.</summary>
        /// <returns>A queryable source of PowerNine cards</returns>
        IQueryable<Core.DTOs.PowerNineCard> GetPowerNineCards();

        /// <summary>
        ///  Removes all the cards.
        /// </summary>
        void RemoveAllCards();

        /// <summary>Updates the cards using the specified file URI.</summary>
        /// <param name="fileURI">The card list file URI.</param>
        void UpdateCardsFromFile(string fileURI);
        #endregion
    }
}
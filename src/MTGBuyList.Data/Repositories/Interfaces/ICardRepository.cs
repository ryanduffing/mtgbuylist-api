﻿// -------------------------------------------------------------------------------
// <copyright file="ICardRepository.cs" company="Ryan Duffing">
//   Copyright (c) 2015 All Rights Reserved
//   <author>Ryan Duffing</author>
// </copyright>
// -------------------------------------------------------------------------------

namespace MTGBuyList.Data.Repositories.Interfaces
{
    using MTGBuyList.Data.Entities;

    /// <summary>
    /// The Card Repository interface
    /// </summary>
    public interface ICardRepository : IGenericRepository<Card>
    {
        #region Methods
        /// <summary>Deletes all card entities.</summary>
        /// <returns>A value indicating whether the delete was successful.</returns>
        bool DeleteAllCards();
        #endregion
    }
}
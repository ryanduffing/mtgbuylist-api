﻿// -------------------------------------------------------------------------------
// <copyright file="ColorService.cs" company="Ryan Duffing">
//   Copyright (c) 2015 All Rights Reserved
//   <author>Ryan Duffing</author>
// </copyright>
// -------------------------------------------------------------------------------

namespace MTGBuyList.Service.Services
{
    using System;
    using System.Linq;
    using AutoMapper.QueryableExtensions;
    using MTGBuyList.Core.Constants;
    using MTGBuyList.Data.Entities;
    using MTGBuyList.Data.Repositories.Interfaces;
    using MTGBuyList.Service.Services.Interfaces;

    /// <summary>
    /// The color service
    /// </summary>
    /// <seealso cref="MTGBuyList.Service.Services.Interfaces.IColorService" />
    public class ColorService : IColorService
    {
        #region Fields
        /// <summary>
        /// The color repository
        /// </summary>
        private readonly IColorRepository colorRepository;
        #endregion

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="ColorService"/> class.
        /// </summary>
        /// <param name="colorRepository">The color repository.</param>
        public ColorService(IColorRepository colorRepository)
        {
            this.colorRepository = colorRepository;
        }
        #endregion

        #region Methods
        /// <summary>
        /// Gets the collection of colors.
        /// </summary>
        /// <returns>The collection of colors</returns>
        public IQueryable<Core.DTOs.Color> GetColors()
        {
            return this.colorRepository.GetAll().Project().To<Core.DTOs.Color>();
        }

        /// <summary>
        /// Removes all colors from the data repository.
        /// </summary>
        public void RemoveAllColors()
        {
            this.colorRepository.DeleteAllColors();
            this.colorRepository.Save();
        }

        /// <summary>
        /// Updates all the colors in the data repository.
        /// </summary>
        public void UpdateAllColors()
        {
            var colors = new string[]
            {
                ColorConstants.White,
                ColorConstants.Blue,
                ColorConstants.Black,
                ColorConstants.Red,
                ColorConstants.Green,
                ColorConstants.Gold,
                ColorConstants.Artifact,
                ColorConstants.Colorless,
                ColorConstants.Land
            };

            var colorList = colors.Select(
                color => new Color
                {
                    Name = color
                }).ToList();

            this.colorRepository.AddMany(colorList);
            this.colorRepository.Save();
        }

        /// <summary>
        /// Gets the color of the card.
        /// </summary>
        /// <param name="cardColors">The card colors.</param>
        /// <param name="cardTypes">The card types.</param>
        /// <returns>The color of the card</returns>
        public string GetCardColor(string[] cardColors, string[] cardTypes)
        {
            string cardColor;

            if (cardTypes != null && cardTypes.Contains(ColorConstants.Land, StringComparer.CurrentCultureIgnoreCase))
            {
                // Card is a land
                cardColor = ColorConstants.Land;
            }
            else if (cardColors != null && cardColors.Length > 0)
            {
                // Card has a color: white, blue, black, red, green OR gold
                cardColor = cardColors.Length == 1 ? cardColors[0] : ColorConstants.Gold;
            }
            else
            {
                // Card has no color
                if (cardTypes != null && cardTypes.Contains(ColorConstants.Artifact, StringComparer.CurrentCultureIgnoreCase))
                {
                    // Card is an artifact
                    cardColor = ColorConstants.Artifact;
                }
                else
                {
                    // Card is colorless
                    cardColor = ColorConstants.Colorless;
                }
            }

            return cardColor;
        }

        /// <summary>Gets the card color from PowerNine's styling logic.</summary>
        /// <param name="backgroundColor">The background color.</param>
        /// <param name="fontColor">The font color.</param>
        /// <returns>The card's color.</returns>
        public string GetCardColor(string backgroundColor, string fontColor)
        {
            string cardColor = null;

            switch (backgroundColor.ToLower())
            {
                case "white":
                    switch (fontColor.ToLower())
                    {
                        case "blue":
                            cardColor = ColorConstants.Blue;
                            break;

                        case "black":
                            cardColor = ColorConstants.Black;
                            break;

                        case "red":
                            cardColor = ColorConstants.Red;
                            break;

                        case "green":
                            cardColor = ColorConstants.Green;
                            break;

                        case "brown":
                            cardColor = ColorConstants.Artifact;
                            break;
                    }

                    break;

                case "grey":
                    switch (fontColor.ToLower())
                    {
                        case "black":
                            cardColor = ColorConstants.Land;
                            break;
                    }

                    break;

                case "black":
                    switch (fontColor.ToLower())
                    {
                        case "white":
                            cardColor = ColorConstants.White;
                            break;

                        case "gold":
                            cardColor = ColorConstants.Gold;
                            break;

                        case "grey":
                            cardColor = ColorConstants.Colorless;
                            break;
                    }

                    break;
            }

            return cardColor;
        }
        #endregion
    }
}
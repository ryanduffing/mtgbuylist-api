﻿// -------------------------------------------------------------------------------
// <copyright file="Color.cs" company="Ryan Duffing">
//   Copyright (c) 2015 All Rights Reserved
//   <author>Ryan Duffing</author>
// </copyright>
// -------------------------------------------------------------------------------

namespace MTGBuyList.Core.DTOs
{
    /// <summary>
    /// The color data transfer object.
    /// </summary>
    public class Color
    {
        #region Properties
        /// <summary>Gets or sets the color identifier.</summary>
        /// <value>The color identifier.</value>
        public int ColorId { get; set; }

        /// <summary>Gets or sets the name.</summary>
        /// <value>The name.</value>
        public string Name { get; set; }
        #endregion
    }
}
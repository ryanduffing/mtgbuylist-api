﻿// -------------------------------------------------------------------------------
// <copyright file="HelperController.cs" company="Ryan Duffing">
//   Copyright (c) 2015 All Rights Reserved
//   <author>Ryan Duffing</author>
// </copyright>
// -------------------------------------------------------------------------------

namespace MTGBuyList.Web.Controllers
{
    using System.Diagnostics;
    using System.Net;
    using System.Net.Http;
    using System.Web.Http;
    using MTGBuyList.Service.Services.Interfaces;

    /// <summary>
    /// The helper controller which loads the cards into the data repository.
    /// </summary>
    /// <seealso cref="System.Web.Http.ApiController" />
    public class HelperController : ApiController
    {
        #region Fields
        /// <summary>The card service</summary>
        private readonly ICardService cardService;

        /// <summary>The color service</summary>
        private readonly IColorService colorService;

        /// <summary>The set service</summary>
        private readonly ISetService setService;

        /// <summary>The PowerNine service</summary>
        private readonly IPowerNineService powerNineService;

        /// <summary>The rarity service.</summary>
        private readonly IRarityService rarityService;
        #endregion

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="HelperController" /> class.
        /// </summary>
        /// <param name="cardService">The card service.</param>
        /// <param name="colorService">The color service.</param>
        /// <param name="setService">The set service.</param>
        /// <param name="powerNineService">The PowerNine service.</param>
        /// <param name="rarityService">The rarity service.</param>
        public HelperController(ICardService cardService, IColorService colorService, ISetService setService, IPowerNineService powerNineService, IRarityService rarityService)
        {
            this.cardService = cardService;
            this.colorService = colorService;
            this.setService = setService;
            this.powerNineService = powerNineService;
            this.rarityService = rarityService;
        }
        #endregion

        #region Methods
        /// <summary>
        /// Updates the collection of cards in the underlying data store.
        /// </summary>
        /// <returns>
        /// No content.
        /// </returns>
        public HttpResponseMessage Put()
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();

            // Remove all existing PowerNine records, cards, colors, and sets from the data repository
            this.powerNineService.RemovePowerNineRecords();
            this.cardService.RemoveAllCards();
            this.colorService.RemoveAllColors();
            this.rarityService.RemoveAllRarities();
            this.setService.RemoveAllSets();

            // Create all colors, sets, cards, and pricing/misc information
            this.colorService.UpdateAllColors();
            this.rarityService.UpdateAllRarities();
            this.cardService.UpdateCardsFromFile(System.Web.Hosting.HostingEnvironment.MapPath(@"~/App_Data/AllSets.json"));
            this.powerNineService.UpdatePowerNineRecords("http://inventory.powernine.com/buylist/full");

            stopwatch.Stop();

            var message = new HttpResponseMessage
            {
                StatusCode = HttpStatusCode.OK,
                Content = new StringContent($"Time elapsed: {stopwatch.Elapsed:hh\\:mm\\:ss}")
            };

            return message;
        }
        #endregion
    }
}
﻿// -------------------------------------------------------------------------------
// <copyright file="IRarityService.cs" company="Ryan Duffing">
//   Copyright (c) 2015 All Rights Reserved
//   <author>Ryan Duffing</author>
// </copyright>
// -------------------------------------------------------------------------------

namespace MTGBuyList.Service.Services.Interfaces
{
    using System.Linq;

    /// <summary>
    ///  The rarity service interface.
    /// </summary>
    public interface IRarityService
    {
        /// <summary>
        /// Gets the collection of rarity types.
        /// </summary>
        /// <returns>The collection of rarity types</returns>
        IQueryable<Core.DTOs.Rarity> GetRarities();

        /// <summary>
        /// Removes all rarity types from the data repository.
        /// </summary>
        void RemoveAllRarities();

        /// <summary>
        /// Updates all the rarity types in the data repository.
        /// </summary>
        void UpdateAllRarities();
    }
}

﻿// -------------------------------------------------------------------------------
// <copyright file="CardRepository.cs" company="Ryan Duffing">
//   Copyright (c) 2015 All Rights Reserved
//   <author>Ryan Duffing</author>
// </copyright>
// -------------------------------------------------------------------------------

namespace MTGBuyList.Data.Repositories
{
    using System;
    using MTGBuyList.Data.Context;
    using MTGBuyList.Data.Entities;
    using MTGBuyList.Data.Repositories.Interfaces;

    /// <summary>
    /// The Card Repository
    /// </summary>
    public class CardRepository : GenericRepository<Card>, ICardRepository
    {
        #region Fields
        /// <summary>The database context</summary>
        private readonly IMTGBuyListContext context;
        #endregion

        #region Constructors
        /// <summary>Initializes a new instance of the <see cref="CardRepository"/> class.</summary>
        /// <param name="context">The context.</param>
        public CardRepository(IMTGBuyListContext context)
            : base(context)
        {
            this.context = context;
        }
        #endregion

        #region Methods
        /// <summary>Deletes all card entities.</summary>
        /// <returns>A value indicating whether the delete was successful.</returns>
        public bool DeleteAllCards()
        {
            try
            {
                this.context.Database.ExecuteSqlCommand("DELETE Cards");
                return true;
            }
            catch (Exception)
            {
                // TODO log this error
                return false;
            }
        }
        #endregion
    }
}
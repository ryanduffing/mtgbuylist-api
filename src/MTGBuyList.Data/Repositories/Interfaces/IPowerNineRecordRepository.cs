// -------------------------------------------------------------------------------
// <copyright file="IPowerNineRecordRepository.cs" company="Ryan Duffing">
//   Copyright (c) 2015 All Rights Reserved
//   <author>Ryan Duffing</author>
// </copyright>
// -------------------------------------------------------------------------------

namespace MTGBuyList.Data.Repositories.Interfaces
{
    using MTGBuyList.Data.Entities;

    /// <summary>
    /// The PowerNineRecord repository.
    /// </summary>
    public interface IPowerNineRecordRepository : IGenericRepository<PowerNineRecord>
    {
        #region Methods
        /// <summary>Deletes all PowerNineRecord entities.</summary>
        /// <returns>A value indicating whether the delete was successful.</returns>
        bool DeleteAllPowerNineRecords();
        #endregion
    }
}
﻿// -------------------------------------------------------------------------------
// <copyright file="IRarityRepository.cs" company="Ryan Duffing">
//   Copyright (c) 2015 All Rights Reserved
//   <author>Ryan Duffing</author>
// </copyright>
// -------------------------------------------------------------------------------

namespace MTGBuyList.Data.Repositories.Interfaces
{
    using MTGBuyList.Data.Entities;

    /// <summary>
    ///  The rarity repository interface.
    /// </summary>
    public interface IRarityRepository : IGenericRepository<Rarity>
    {
        /// <summary>Deletes all rarity entities.</summary>
        /// <returns>A value indicating whether the delete was successful.</returns>
        bool DeleteAllRarities();
    }
}

﻿// -------------------------------------------------------------------------------
// <copyright file="RarityConstants.cs" company="Ryan Duffing">
//   Copyright (c) 2016 All Rights Reserved
//   <author>Duffing, Ryan</author>
// </copyright>
// -------------------------------------------------------------------------------

namespace MTGBuyList.Core.Constants
{
    /// <summary>
    ///  Constants for different types of rarities.
    /// </summary>
    public class RarityConstants
    {
        /// <summary>
        ///  Mythic rare rarity.
        /// </summary>
        public const string MythicRare = "Mythic Rare";

        /// <summary>
        ///  Rare rarity.
        /// </summary>
        public const string Rare = "Rare";

        /// <summary>
        ///  Uncommon rarity.
        /// </summary>
        public const string Uncommon = "Uncommon";

        /// <summary>
        ///  Common rarity.
        /// </summary>
        public const string Common = "Common";

        /// <summary>
        ///  Time-shifted rarity.
        /// </summary>
        public const string Timeshifted = "Timeshifted";

        /// <summary>
        ///  Special rarity.
        /// </summary>
        public const string Special = "Special";

        /// <summary>
        ///  Basic land rarity.
        /// </summary>
        public const string BasicLand = "Basic Land";
    }
}

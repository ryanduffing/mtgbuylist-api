﻿// -------------------------------------------------------------------------------
// <copyright file="FilterConfig.cs" company="Ryan Duffing">
//   Copyright (c) 2016 All Rights Reserved
//   <author>Duffing, Ryan</author>
// </copyright>
// -------------------------------------------------------------------------------

namespace MTGBuyList.Web
{
    using System.Web.Mvc;

    /// <summary>
    ///  Configures the filters.
    /// </summary>
    public class FilterConfig
    {
        /// <summary>
        ///  Register any global filters.
        /// </summary>
        /// <param name="filters">Collection of filters.</param>
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}

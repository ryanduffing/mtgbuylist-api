// -------------------------------------------------------------------------------
// <copyright file="IWebFileReader.cs" company="Ryan Duffing">
//   Copyright (c) 2015 All Rights Reserved
//   <author>Ryan Duffing</author>
// </copyright>
// -------------------------------------------------------------------------------

namespace MTGBuyList.Service.Services.Interfaces
{
    /// <summary>
    /// Reads a file from the web.
    /// </summary>
    public interface IWebFileReader
    {
        #region Methods
        /// <summary>Reads the web file.</summary>
        /// <param name="fileURI">The file URI.</param>
        /// <returns>A string containing the contents of the web file.</returns>
        string ReadWebFile(string fileURI);
        #endregion
    }
}
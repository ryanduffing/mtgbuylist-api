﻿// -------------------------------------------------------------------------------
// <copyright file="Color.cs" company="Ryan Duffing">
//   Copyright (c) 2015 All Rights Reserved
//   <author>Ryan Duffing</author>
// </copyright>
// -------------------------------------------------------------------------------

namespace MTGBuyList.Data.Entities
{
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// The Color data entity
    /// </summary>
    public class Color
    {
        #region Properties
        /// <summary>Gets or sets the color identifier.</summary>
        /// <value>The color identifier.</value>
        [Key]
        public int ColorId { get; set; }

        /// <summary>Gets or sets the color name.</summary>
        /// <value>The color name.</value>
        public string Name { get; set; }
        #endregion
    }
}
﻿// -------------------------------------------------------------------------------
// <copyright file="SetController.cs" company="Ryan Duffing">
//   Copyright (c) 2016 All Rights Reserved
//   <author>Duffing, Ryan</author>
// </copyright>
// -------------------------------------------------------------------------------

using System.Web.Http;

namespace MTGBuyList.Web.Controllers.ODataControllers
{
    using System.Linq;
    using System.Web.OData;
    using MTGBuyList.Core.DTOs;
    using MTGBuyList.Service.Services.Interfaces;

    /// <summary>
    /// The OData controller for accessing PowerNine sets.
    /// </summary>
    public class SetController : ODataController
    {
        #region Fields
        /// <summary>The set service</summary>
        private readonly ISetService setService;
        #endregion

        #region Constructors
        /// <summary>Initializes a new instance of the <see cref="SetController" /> class.</summary>
        /// <param name="setService">The PowerNine set service.</param>
        public SetController(ISetService setService)
        {
            this.setService = setService;
        }
        #endregion

        #region Methods
        [EnableQuery(AllowedQueryOptions = System.Web.OData.Query.AllowedQueryOptions.All)]
        public SingleResult<Set> Get([FromODataUri] int key)
        {
            var result = this.setService.GetSetById(key);

            return SingleResult.Create(result);
        }

        /// <summary>Gets the collection of PowerNine cards.</summary>
        /// <returns>The collection of PowerNine cards.</returns>
        [EnableQuery(AllowedQueryOptions = System.Web.OData.Query.AllowedQueryOptions.All)]
        public IQueryable<Set> Get()
        {
            return this.setService.GetSets();
        }
        #endregion
    }
}
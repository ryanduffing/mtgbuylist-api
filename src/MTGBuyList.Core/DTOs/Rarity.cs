﻿// -------------------------------------------------------------------------------
// <copyright file="Rarity.cs" company="Ryan Duffing">
//   Copyright (c) 2015 All Rights Reserved
//   <author>Ryan Duffing</author>
// </copyright>
// -------------------------------------------------------------------------------

namespace MTGBuyList.Core.DTOs
{
    /// <summary>
    ///  The rarity DTO class.
    /// </summary>
    public class Rarity
    {
        /// <summary>
        ///  Gets or sets the unique identifier for the rarity.
        /// </summary>
        public int RarityId { get; set; }

        /// <summary>
        ///  Gets or sets the name for the rarity.
        /// </summary>
        public string Name { get; set; }
    }
}

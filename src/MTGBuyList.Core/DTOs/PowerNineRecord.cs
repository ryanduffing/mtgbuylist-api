﻿// -------------------------------------------------------------------------------
// <copyright file="PowerNineRecord.cs" company="Ryan Duffing">
//   Copyright (c) 2015 All Rights Reserved
//   <author>Ryan Duffing</author>
// </copyright>
// -------------------------------------------------------------------------------

namespace MTGBuyList.Core.DTOs
{
    /// <summary>
    /// The card price data transfer object.
    /// </summary>
    public class PowerNineRecord
    {
        #region Properties
        /// <summary>Gets or sets the PowerNine record identifier.</summary>
        /// <value>The PowerNine record identifier.</value>
        public int PowerNineRecordId { get; set; }

        /// <summary>Gets or sets the card identifier.</summary>
        /// <value>The card identifier.</value>
        public int? CardId { get; set; }

        /// <summary>Gets or sets the buy price.</summary>
        /// <value>The buy price.</value>
        public double BuyPrice { get; set; }

        /// <summary>Gets or sets the PowerNine card name.</summary>
        /// <value>The PowerNine card name.</value>
        public string PowerNineName { get; set; }

        /// <summary>Gets or sets a value indicating whether the price is for the foil version of the card.</summary>
        /// <value>A value indicating whether the price is for the foil version of the card.</value>
        public bool IsFoil { get; set; }

        /// <summary>Gets or sets the card.</summary>
        /// <value>The card.</value>
        public Card Card { get; set; }
        #endregion
    }
}
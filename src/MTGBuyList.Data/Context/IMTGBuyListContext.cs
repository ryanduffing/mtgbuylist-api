﻿// -------------------------------------------------------------------------------
// <copyright file="IMTGBuyListContext.cs" company="Ryan Duffing">
//   Copyright (c) 2015 All Rights Reserved
//   <author>Ryan Duffing</author>
// </copyright>
// -------------------------------------------------------------------------------

namespace MTGBuyList.Data.Context
{
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;

    /// <summary>
    /// The MTG Buy List database context interface
    /// </summary>
    public interface IMTGBuyListContext
    {
        #region Properties
        /// <summary>
        ///  Gets the database object.
        /// </summary>
        Database Database { get; }
        #endregion

        #region Methods
        /// <summary>Gets the DbSet for the specified entity type.</summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <returns>The DbSet for the specified entity type.</returns>
        DbSet<TEntity> Set<TEntity>() where TEntity : class;

        /// <summary>Gets the DbEntityEntry for the specified entity type.</summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <param name="entity">The entity.</param>
        /// <returns>The DbEntityEntry for the specified entity type.</returns>
        DbEntityEntry<TEntity> Entry<TEntity>(TEntity entity) where TEntity : class;

        /// <summary>Saves all changes made in this context to the underlying database.</summary>
        /// <returns>
        /// The number of state entries written to the underlying database. This can include
        /// state entries for entities and/or relationships. Relationship state entries are created for
        /// many-to-many relationships and relationships where there is no foreign key property
        /// included in the entity class (often referred to as independent associations).
        /// </returns>
        int SaveChanges();
        #endregion
    }
}
﻿// -------------------------------------------------------------------------------
// <copyright file="Rarity.cs" company="Ryan Duffing">
//   Copyright (c) 2015 All Rights Reserved
//   <author>Ryan Duffing</author>
// </copyright>
// -------------------------------------------------------------------------------

namespace MTGBuyList.Data.Entities
{
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    ///  The rarity entity.
    /// </summary>
    public class Rarity
    {
        /// <summary>
        ///  Gets or sets the unique identifier for the rarity.
        /// </summary>
        [Key]
        public int RarityId { get; set; }

        /// <summary>
        ///  Gets or sets the name for the rarity.
        /// </summary>
        public string Name { get; set; }
    }
}

﻿// -------------------------------------------------------------------------------
// <copyright file="ColorController.cs" company="Ryan Duffing">
//   Copyright (c) 2016 All Rights Reserved
//   <author>Duffing, Ryan</author>
// </copyright>
// -------------------------------------------------------------------------------

namespace MTGBuyList.Web.Controllers.ODataControllers
{
    using System.Linq;
    using System.Web.OData;
    using MTGBuyList.Core.DTOs;
    using MTGBuyList.Service.Services.Interfaces;

    /// <summary>
    /// The OData controller for accessing colors.
    /// </summary>
    public class ColorController : ODataController
    {
        #region Fields
        /// <summary>The color service</summary>
        private readonly IColorService colorService;
        #endregion

        #region Constructors
        /// <summary>Initializes a new instance of the <see cref="ColorController" /> class.</summary>
        /// <param name="colorService">The color service.</param>
        public ColorController(IColorService colorService)
        {
            this.colorService = colorService;
        }
        #endregion

        #region Methods
        /// <summary>Gets the collection of color.</summary>
        /// <returns>The collection of colors.</returns>
        [EnableQuery(AllowedQueryOptions = System.Web.OData.Query.AllowedQueryOptions.All)]
        public IQueryable<Color> Get()
        {
            return this.colorService.GetColors();
        }
        #endregion
    }
}
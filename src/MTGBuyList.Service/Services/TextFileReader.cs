﻿// -------------------------------------------------------------------------------
// <copyright file="TextFileReader.cs" company="Ryan Duffing">
//   Copyright (c) 2015 All Rights Reserved
//   <author>Ryan Duffing</author>
// </copyright>
// -------------------------------------------------------------------------------

namespace MTGBuyList.Service.Services
{
    using System.IO;
    using MTGBuyList.Service.Services.Interfaces;

    /// <summary>
    /// Class for reading text files.
    /// </summary>
    public class TextFileReader : ITextFileReader
    {
        #region Methods
        /// <summary>Reads the web file.</summary>
        /// <param name="fileURI">The file URI.</param>
        /// <returns>A string containing the contents of the text file.</returns>
        public string ReadFile(string fileURI)
        {
            var fileContents = string.Empty;

            if (File.Exists(fileURI))
            {
                fileContents = File.ReadAllText(fileURI);
            }

            return fileContents;
        }
        #endregion
    }
}
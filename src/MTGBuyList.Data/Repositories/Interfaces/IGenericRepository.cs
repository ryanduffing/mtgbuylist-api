﻿// -------------------------------------------------------------------------------
// <copyright file="IGenericRepository.cs" company="Ryan Duffing">
//   Copyright (c) 2015 All Rights Reserved
//   <author>Ryan Duffing</author>
// </copyright>
// -------------------------------------------------------------------------------

namespace MTGBuyList.Data.Repositories.Interfaces
{
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    ///  The generic repository interface.
    /// </summary>
    /// <typeparam name="TEntity">Entity repository is being created for.</typeparam>
    public interface IGenericRepository<TEntity>
        where TEntity : class
    {
        #region Methods
        /// <summary>Gets all of the entities.</summary>
        /// <returns>A queryable source of entities</returns>
        IQueryable<TEntity> GetAll();

        /// <summary>Gets the entity with the specified identifier.</summary>
        /// <param name="id">The identifier.</param>
        /// <returns>The entity with the specified identifier.</returns>
        IQueryable<TEntity> Get(object id);

        /// <summary>Adds many entities.</summary>
        /// <param name="entities">The entity collection.</param>
        /// <returns>A value indicating whether the add was successful.</returns>
        bool AddMany(IEnumerable<TEntity> entities);

        /// <summary>Adds the entity.</summary>
        /// <param name="entity">The entity.</param>
        /// <returns>A value indicating whether the add was successful.</returns>
        bool Add(TEntity entity);

        /// <summary>Deletes the entity with the specified identifier.</summary>
        /// <param name="id">The identifier.</param>
        void Delete(object id);

        /// <summary>Deletes the specified entity.</summary>
        /// <param name="entity">The entity.</param>
        void Delete(TEntity entity);

        /// <summary>
        ///  Updates the specified entity.
        /// </summary>
        /// <param name="entity">The entity</param>
        /// <param name="id">Primary key of the entity.</param>
        void Update(TEntity entity, object id);

        /// <summary>Saves the changes.</summary>
        /// <returns>A value indicating whether the save was successful.</returns>
        bool Save();
        #endregion
    }
}
﻿// -------------------------------------------------------------------------------
// <copyright file="PowerNineRecordRepository.cs" company="Ryan Duffing">
//   Copyright (c) 2015 All Rights Reserved
//   <author>Ryan Duffing</author>
// </copyright>
// -------------------------------------------------------------------------------

namespace MTGBuyList.Data.Repositories
{
    using System;
    using MTGBuyList.Data.Context;
    using MTGBuyList.Data.Entities;
    using MTGBuyList.Data.Repositories.Interfaces;

    /// <summary>
    /// The PowerNine Record Repository
    /// </summary>
    public class PowerNineRecordRepository : GenericRepository<PowerNineRecord>, IPowerNineRecordRepository
    {
        #region Fields
        /// <summary>The database context</summary>
        private readonly IMTGBuyListContext context;
        #endregion

        #region Constructors
        /// <summary>Initializes a new instance of the <see cref="PowerNineRecordRepository"/> class.</summary>
        /// <param name="context">The context.</param>
        public PowerNineRecordRepository(IMTGBuyListContext context)
            : base(context)
        {
            this.context = context;
        }
        #endregion

        #region Methods
        /// <summary>Deletes all PowerNineRecord entities.</summary>
        /// <returns>A value indicating whether the delete was successful.</returns>
        public bool DeleteAllPowerNineRecords()
        {
            try
            {
                this.context.Database.ExecuteSqlCommand("DELETE PowerNineRecords");
                return true;
            }
            catch (Exception)
            {
                // TODO log this error
                return false;
            }
        }
        #endregion
    }
}
// -------------------------------------------------------------------------------
// <copyright file="ITextFileReader.cs" company="Ryan Duffing">
//   Copyright (c) 2015 All Rights Reserved
//   <author>Ryan Duffing</author>
// </copyright>
// -------------------------------------------------------------------------------

namespace MTGBuyList.Service.Services.Interfaces
{
    /// <summary>
    /// Interface for reading text files
    /// </summary>
    public interface ITextFileReader
    {
        #region Methods
        /// <summary>Reads the web file.</summary>
        /// <param name="fileURI">The file URI.</param>
        /// <returns>A string containing the contents of the text file.</returns>
        string ReadFile(string fileURI);
        #endregion
    }
}
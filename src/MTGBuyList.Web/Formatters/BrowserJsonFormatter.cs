﻿// -------------------------------------------------------------------------------
// <copyright file="BrowserJsonFormatter.cs" company="Ryan Duffing">
//   Copyright (c) 2015 All Rights Reserved
//   <author>Ryan Duffing</author>
// </copyright>
// -------------------------------------------------------------------------------

namespace MTGBuyList.Web.Formatters
{
    using System;
    using System.Net.Http.Formatting;
    using System.Net.Http.Headers;
    using Newtonsoft.Json;

    /// <summary>The browser json formatter.</summary>
    public class BrowserJsonFormatter : JsonMediaTypeFormatter
    {
        #region Constructors
        /// <summary>Initializes a new instance of the <see cref="BrowserJsonFormatter"/> class.</summary>
        public BrowserJsonFormatter()
        {
            this.SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/html"));
            this.SerializerSettings.Formatting = Formatting.Indented;
        }
        #endregion

        #region Methods
        /// <summary>The set default content headers.</summary>
        /// <param name="type">The type.</param>
        /// <param name="headers">The headers.</param>
        /// <param name="mediaType">The media type.</param>
        public override void SetDefaultContentHeaders(Type type, HttpContentHeaders headers, MediaTypeHeaderValue mediaType)
        {
            base.SetDefaultContentHeaders(type, headers, mediaType);
            headers.ContentType = new MediaTypeHeaderValue("application/json");
        }
        #endregion
    }
}
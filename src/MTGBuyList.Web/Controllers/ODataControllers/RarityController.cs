﻿// -------------------------------------------------------------------------------
// <copyright file="RarityController.cs" company="Ryan Duffing">
//   Copyright (c) 2016 All Rights Reserved
//   <author>Duffing, Ryan</author>
// </copyright>
// -------------------------------------------------------------------------------

namespace MTGBuyList.Web.Controllers.ODataControllers
{
    using System.Linq;
    using System.Web.OData;
    using MTGBuyList.Core.DTOs;
    using MTGBuyList.Service.Services.Interfaces;

    /// <summary>
    /// The OData controller for accessing rarities.
    /// </summary>
    public class RarityController : ODataController
    {
        #region Fields
        /// <summary>The rarity service</summary>
        private readonly IRarityService rarityService;
        #endregion

        #region Constructors
        /// <summary>Initializes a new instance of the <see cref="RarityController" /> class.</summary>
        /// <param name="rarityService">The rarity service.</param>
        public RarityController(IRarityService rarityService)
        {
            this.rarityService = rarityService;
        }
        #endregion

        #region Methods
        /// <summary>Gets the collection of rarities.</summary>
        /// <returns>The collection of rarities.</returns>
        [EnableQuery(AllowedQueryOptions = System.Web.OData.Query.AllowedQueryOptions.All)]
        public IQueryable<Rarity> Get()
        {
            return this.rarityService.GetRarities();
        }
        #endregion
    }
}
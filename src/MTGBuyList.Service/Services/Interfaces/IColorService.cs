﻿// -------------------------------------------------------------------------------
// <copyright file="IColorService.cs" company="Ryan Duffing">
//   Copyright (c) 2015 All Rights Reserved
//   <author>Ryan Duffing</author>
// </copyright>
// -------------------------------------------------------------------------------

namespace MTGBuyList.Service.Services.Interfaces
{
    using System.Linq;

    /// <summary>
    ///  The color service interface.
    /// </summary>
    public interface IColorService
    {
        /// <summary>
        /// Gets the collection of colors.
        /// </summary>
        /// <returns>The collection of colors</returns>
        IQueryable<Core.DTOs.Color> GetColors();

        /// <summary>
        /// Removes all colors from the data repository.
        /// </summary>
        void RemoveAllColors();

        /// <summary>
        /// Updates all the colors in the data repository.
        /// </summary>
        void UpdateAllColors();

        /// <summary>
        /// Gets the color of the card.
        /// </summary>
        /// <param name="cardColors">The card colors.</param>
        /// <param name="cardTypes">The card types.</param>
        /// <returns>The color of the card</returns>
        string GetCardColor(string[] cardColors, string[] cardTypes);

        /// <summary>Gets the card color from PowerNine's styling logic.</summary>
        /// <param name="backgroundColor">The background color.</param>
        /// <param name="fontColor">The font color.</param>
        /// <returns>The card's color.</returns>
        string GetCardColor(string backgroundColor, string fontColor);
    }
}

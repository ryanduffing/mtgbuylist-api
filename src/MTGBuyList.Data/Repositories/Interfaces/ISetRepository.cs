﻿// -------------------------------------------------------------------------------
// <copyright file="ISetRepository.cs" company="Ryan Duffing">
//   Copyright (c) 2015 All Rights Reserved
//   <author>Ryan Duffing</author>
// </copyright>
// -------------------------------------------------------------------------------

namespace MTGBuyList.Data.Repositories.Interfaces
{
    using MTGBuyList.Data.Entities;

    /// <summary>
    /// The set repository
    /// </summary>
    public interface ISetRepository : IGenericRepository<Set>
    {
        #region Methods
        /// <summary>Deletes all set entities.</summary>
        /// <returns>A value indicating whether the delete was successful.</returns>
        bool DeleteAllSets();
        #endregion
    }
}
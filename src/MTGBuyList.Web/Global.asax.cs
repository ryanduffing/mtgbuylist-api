﻿// -------------------------------------------------------------------------------
// <copyright file="Global.asax.cs" company="Ryan Duffing">
//   Copyright (c) 2016 All Rights Reserved
//   <author>Duffing, Ryan</author>
// </copyright>
// -------------------------------------------------------------------------------

namespace MTGBuyList.Web
{
    using System.Web;
    using System.Web.Http;

    /// <summary>
    ///  Global class.
    /// </summary>
    public class WebApiApplication : HttpApplication
    {
        /// <summary>
        ///  Runs when the application starts up.
        /// </summary>
        #region Methods
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(ODataConfig.Register);
            GlobalConfiguration.Configure(WebApiConfig.Register);
            AutoMapperConfig.RegisterMappings();
        }
        #endregion
    }
}
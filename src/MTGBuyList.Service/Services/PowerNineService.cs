﻿// -------------------------------------------------------------------------------
// <copyright file="PowerNineService.cs" company="Ryan Duffing">
//   Copyright (c) 2015 All Rights Reserved
//   <author>Ryan Duffing</author>
// </copyright>
// -------------------------------------------------------------------------------

namespace MTGBuyList.Service.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using AutoMapper.QueryableExtensions;
    using HtmlAgilityPack;
    using MTGBuyList.Core.Constants;
    using MTGBuyList.Data.Entities;
    using MTGBuyList.Data.Repositories.Interfaces;
    using MTGBuyList.Service.Services.Interfaces;

    /// <summary>The PowerNine service for updating card buy prices and other information.</summary>
    public class PowerNineService : IPowerNineService
    {
        #region Fields
        /// <summary>The PowerNine record repository for accessing data</summary>
        private readonly IPowerNineRecordRepository powerNineRecordRepository;

        /// <summary>The Set repository for accessing set data</summary>
        private readonly ISetRepository setRepository;

        /// <summary>The web file reader for reading files on the web</summary>
        private readonly IWebFileReader webFileReader;

        /// <summary>The card service</summary>
        private readonly ICardService cardService;

        /// <summary>The color service</summary>
        private readonly IColorService colorService;

        /// <summary>The set service</summary>
        private readonly ISetService setService;
        #endregion

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="PowerNineService"/> class.
        /// </summary>
        /// <param name="powerNineRecordRepository">The PowerNine record repository.</param>
        /// <param name="setRepository">The Set repository.</param>
        /// <param name="webFileReader">The web file reader.</param>
        /// <param name="cardService">The card service.</param>
        /// <param name="colorService">The color service.</param>
        /// <param name="setService">The set service.</param>
        public PowerNineService(
            IPowerNineRecordRepository powerNineRecordRepository,
            ISetRepository setRepository,
            IWebFileReader webFileReader,
            ICardService cardService,
            IColorService colorService,
            ISetService setService)
        {
            this.powerNineRecordRepository = powerNineRecordRepository;
            this.setRepository = setRepository;
            this.webFileReader = webFileReader;
            this.cardService = cardService;
            this.colorService = colorService;
            this.setService = setService;
        }
        #endregion

        #region Methods
        /// <summary>Removes all PowerNine records.</summary>
        public void RemovePowerNineRecords()
        {
            this.powerNineRecordRepository.DeleteAllPowerNineRecords();
            this.powerNineRecordRepository.Save();
        }

        /// <summary>Updates the PowerNine records using the specified file URI.</summary>
        /// <param name="fileURI">The PowerNine card buy list file URI.</param>
        public void UpdatePowerNineRecords(string fileURI)
        {
            var powerNineRecordList = new List<PowerNineRecord>();
            var recordsWithoutCards = new List<PowerNineRecord>();

            var cardList = this.cardService.GetCards().ToList();
            var setList = this.setService.GetSets().ToList();
            var pricesHTML = this.webFileReader.ReadWebFile(fileURI);

            var htmlDocument = new HtmlDocument();
            htmlDocument.LoadHtml(pricesHTML);

            // Get table rows
            var rows = htmlDocument.DocumentNode.SelectNodes("//tr").Where(tr => tr.HasChildNodes && tr.ChildNodes.Count(cn => cn.Name == "td") == 6);

            foreach (var row in rows)
            {
                var cells = row.ChildNodes.Where(cn => cn.Name == "td").ToList();

                var styles = cells[3].Attributes["style"].Value.Split(';');
                var backgroundColor = styles[0].Split(':')[1].Trim();
                var fontColor = styles[1].Split(':')[1].Trim();
                var cardColor = this.colorService.GetCardColor(backgroundColor, fontColor);

                var price = double.Parse(cells[0].InnerText.Trim().Replace("$", string.Empty));
                var setImageUrl = cells[1].ChildNodes.First(cn => cn.Name == "img").Attributes["src"].Value;
                var foilText = WebUtility.HtmlDecode(cells[2].InnerText).Trim();
                var isFoil = string.Equals(foilText, "FOIL", StringComparison.CurrentCultureIgnoreCase);
                var cardName = WebUtility.HtmlDecode(cells[3].InnerText).Trim();
                var setName = WebUtility.HtmlDecode(cells[5].InnerText).Trim();

                // Set conspiracy override:
                if (string.Equals(setName, "Conspiracy", StringComparison.CurrentCultureIgnoreCase))
                {
                    setName = "Magic: The Gathering—Conspiracy";
                }

                // Get matching Card
                var matchingCard =
                    cardList.FirstOrDefault(
                        c =>
                        string.Equals(c.ASCIIName, cardName, StringComparison.CurrentCultureIgnoreCase)
                        && string.Equals(c.SetName, setName, StringComparison.CurrentCultureIgnoreCase));

                // Check if no matching Card was found
                if (matchingCard == null)
                {
                    // If: Check for set-numbered land (Zendikar) format: [Forest #245]
                    // ElseIf: Check for dual-cards
                    if (string.Equals(cardColor, ColorConstants.Land, StringComparison.CurrentCulture) && cardName.Contains("#"))
                    {
                        var cardNameParts = cardName.Split('#');
                        var landName = cardNameParts[0].Trim();
                        var mciNumber = cardNameParts[1].Trim();
                        matchingCard =
                            cardList.FirstOrDefault(
                                c =>
                                string.Equals(c.SetName, setName, StringComparison.CurrentCultureIgnoreCase)
                                && string.Equals(c.ASCIIName, landName, StringComparison.CurrentCultureIgnoreCase)
                                && string.Equals(c.ColorName, ColorConstants.Land, StringComparison.CurrentCultureIgnoreCase) && c.Number.Contains(mciNumber));

                        // TODO: if cardId still NULL, check for OGW Wastes (4 cards)
                    }
                    else if (cardName.Contains(@"//"))
                    {
                        // Get the first card name for the card name format: [Boom // Bust]
                        var firstCardName = cardName.Substring(0, cardName.IndexOf(@"/", StringComparison.Ordinal)).Trim();

                        matchingCard =
                            cardList.FirstOrDefault(
                                c =>
                                string.Equals(c.ASCIIName, firstCardName, StringComparison.CurrentCultureIgnoreCase)
                                && string.Equals(c.SetName, setName, StringComparison.CurrentCultureIgnoreCase));
                    }
                }

                // Get matching set to update the set image
                if (matchingCard != null)
                {
                    setList.Find(x => x.SetId == matchingCard.Set.SetId).ImageUrl = setImageUrl;
                }

                var powerNineRecord = new PowerNineRecord()
                {
                    PowerNineName = cardName,
                    BuyPrice = price,
                    IsFoil = isFoil,
                    CardId = matchingCard?.CardId
                };

                powerNineRecordList.Add(powerNineRecord);

                // Null color check
                if (powerNineRecord.CardId == null)
                {
                    ////throw new Exception($"No matching Card was found for cardName{cardName}, setName{setName}");
                    recordsWithoutCards.Add(powerNineRecord);
                }
            }

            // Add the new cards to the data store
            this.powerNineRecordRepository.AddMany(powerNineRecordList);
            this.powerNineRecordRepository.Save();

            // Update sets with their set-images
            foreach (var set in setList.Where(x => !string.IsNullOrWhiteSpace(x.ImageUrl)).AsQueryable().Project().To<Set>())
            {
                this.setRepository.Update(set, set.SetId);
            }

            this.setRepository.Save();
        }
        #endregion
    }
}
﻿// -------------------------------------------------------------------------------
// <copyright file="IColorRepository.cs" company="Ryan Duffing">
//   Copyright (c) 2015 All Rights Reserved
//   <author>Ryan Duffing</author>
// </copyright>
// -------------------------------------------------------------------------------

namespace MTGBuyList.Data.Repositories.Interfaces
{
    using MTGBuyList.Data.Entities;

    /// <summary>
    ///  Color repository.
    /// </summary>
    public interface IColorRepository : IGenericRepository<Color>
    {
        #region Methods
        /// <summary>Deletes all color entities.</summary>
        /// <returns>A value indicating whether the delete was successful.</returns>
        bool DeleteAllColors();
        #endregion
    }
}
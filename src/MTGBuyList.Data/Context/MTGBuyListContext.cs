﻿// -------------------------------------------------------------------------------
// <copyright file="MTGBuyListContext.cs" company="Ryan Duffing">
//   Copyright (c) 2015 All Rights Reserved
//   <author>Ryan Duffing</author>
// </copyright>
// -------------------------------------------------------------------------------

namespace MTGBuyList.Data.Context
{
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using MTGBuyList.Data.Entities;
    using MTGBuyList.Data.Migrations;

    /// <summary>
    /// The MTG Buy List context.
    /// </summary>
    /// <seealso cref="System.Data.Entity.DbContext" />
    public class MTGBuyListContext : DbContext, IMTGBuyListContext
    {
        #region Constructors
        /// <summary>Initializes a new instance of the <see cref="MTGBuyListContext"/> class.</summary>
        public MTGBuyListContext()
            : base("DefaultConnection")
        {
            this.Configuration.LazyLoadingEnabled = false; // serialization problems
            this.Configuration.ProxyCreationEnabled = false; // serialization problems

            Database.SetInitializer(new MigrateDatabaseToLatestVersion<MTGBuyListContext, MTGBuyListMigrationsConfiguration>());

#if DEBUG
            ////Uncomment this to see SQL: //this.Database.Log = s => System.Diagnostics.Debug.WriteLine(s); // See the generated SQL
#endif
        }
        #endregion

        #region Properties
        /// <summary>
        /// Creates a Database instance for this context that allows for creation/deletion/existence checks
        /// for the underlying database.
        /// </summary>
        public new Database Database => base.Database;
        #endregion

        #region Methods
        /// <summary>Gets the DbSet for the specified entity type.</summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <returns>The DbSet for the specified entity type.</returns>
        public new IDbSet<TEntity> Set<TEntity>() where TEntity : class
        {
            return base.Set<TEntity>();
        }

        /// <summary>Gets the DbEntityEntry for the specified entity type.</summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <param name="entity">The entity.</param>
        /// <returns>The DbEntityEntry for the specified entity type.</returns>
        public new DbEntityEntry<TEntity> Entry<TEntity>(TEntity entity) where TEntity : class
        {
            return base.Entry<TEntity>(entity);
        }

        /// <summary>Saves all changes made in this context to the underlying database.</summary>
        /// <returns>
        /// The number of state entries written to the underlying database. This can include
        /// state entries for entities and/or relationships. Relationship state entries are created for
        /// many-to-many relationships and relationships where there is no foreign key property
        /// included in the entity class (often referred to as independent associations).
        /// </returns>
        public new int SaveChanges()
        {
            return base.SaveChanges();
        }

        /// <summary>
        /// This method is called when the model for a derived context has been initialized, but
        /// before the model has been locked down and used to initialize the context.  The default
        /// implementation of this method does nothing, but it can be overridden in a derived class
        /// such that the model can be further configured before it is locked down.
        /// </summary>
        /// <param name="modelBuilder">The builder that defines the model for the context being created.</param>
        /// <remarks>
        /// Typically, this method is called only once when the first instance of a derived context
        /// is created.  The model for that context is then cached and is for all further instances of
        /// the context in the app domain.  This caching can be disabled by setting the ModelCaching
        /// property on the given ModelBuilder, but note that this can seriously degrade performance.
        /// More control over caching is provided through use of the DbModelBuilder and DbContextFactory
        /// classes directly.
        /// </remarks>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Card>();
            modelBuilder.Entity<Color>();
            modelBuilder.Entity<Rarity>();
            modelBuilder.Entity<Set>();
            modelBuilder.Entity<PowerNineRecord>();
        }
        #endregion
    }
}
﻿// -------------------------------------------------------------------------------
// <copyright file="StringExtensions.cs" company="Ryan Duffing">
//   Copyright (c) 2015 All Rights Reserved
//   <author>Ryan Duffing</author>
// </copyright>
// -------------------------------------------------------------------------------

namespace MTGBuyList.Core.Extensions
{
    using System;

    /// <summary>
    /// Extension methods for the string type.
    /// </summary>
    public static class StringExtensions
    {
        #region Methods
        /// <summary>Determines whether the source string contains the specified value.</summary>
        /// <param name="source">The source string.</param>
        /// <param name="value">The value to check against.</param>
        /// <param name="comparer">The string comparison method.</param>
        /// <returns>A value indicating whether the source string contains the specified value</returns>
        public static bool Contains(this string source, string value, StringComparison comparer)
        {
            return source.IndexOf(value, comparer) >= 0;
        }
        #endregion
    }
}
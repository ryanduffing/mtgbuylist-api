﻿// -------------------------------------------------------------------------------
// <copyright file="CardController.cs" company="Ryan Duffing">
//   Copyright (c) 2016 All Rights Reserved
//   <author>Duffing, Ryan</author>
// </copyright>
// -------------------------------------------------------------------------------

namespace MTGBuyList.Web.Controllers.ODataControllers
{
    using System.Linq;
    using System.Web.OData;
    using MTGBuyList.Core.DTOs;
    using MTGBuyList.Service.Services.Interfaces;

    /// <summary>
    /// The OData controller for accessing cards.
    /// </summary>
    public class CardController : ODataController
    {
        #region Fields
        /// <summary>The card service</summary>
        private readonly ICardService cardService;
        #endregion

        #region Constructors
        /// <summary>Initializes a new instance of the <see cref="CardController" /> class.</summary>
        /// <param name="cardService">The card service.</param>
        public CardController(ICardService cardService)
        {
            this.cardService = cardService;
        }
        #endregion

        #region Methods
        /// <summary>Gets the collection of cards.</summary>
        /// <returns>The collection of cards.</returns>
        [EnableQuery(AllowedQueryOptions = System.Web.OData.Query.AllowedQueryOptions.All)]
        public IQueryable<Card> Get()
        {
            return this.cardService.GetCards();
        }
        #endregion
    }
}
﻿// -------------------------------------------------------------------------------
// <copyright file="GenericRepository.cs" company="Ryan Duffing">
//   Copyright (c) 2015 All Rights Reserved
//   <author>Ryan Duffing</author>
// </copyright>
// -------------------------------------------------------------------------------

namespace MTGBuyList.Data.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using MTGBuyList.Data.Context;
    using MTGBuyList.Data.Repositories.Interfaces;

    /// <summary>
    ///  The Generic Repository base class
    /// </summary>
    /// <typeparam name="TEntity">Entity to create repository for.</typeparam>
    public abstract class GenericRepository<TEntity> : IGenericRepository<TEntity>
        where TEntity : class
    {
        #region Fields
        /// <summary>The database context</summary>
        private readonly IMTGBuyListContext context;

        /// <summary>
        ///  The database set.
        /// </summary>
        private readonly DbSet<TEntity> dbSet;
        #endregion

        #region Constructors
        /// <summary>Initializes a new instance of the <see cref="GenericRepository{TEntity}"/> class.</summary>
        /// <param name="context">The context.</param>
        public GenericRepository(IMTGBuyListContext context)
        {
            this.context = context;
            this.dbSet = context.Set<TEntity>();
        }
        #endregion

        #region Methods
        /// <summary>Gets all of the entities.</summary>
        /// <returns>A queryable source of entities</returns>
        public IQueryable<TEntity> GetAll()
        {
            return this.dbSet;
        }

        /// <summary>Gets the entity with the specified identifier.</summary>
        /// <param name="id">The identifier.</param>
        /// <returns>The entity with the specified identifier.</returns>
        public IQueryable<TEntity> Get(object id)
        {
            var singleEntity = this.dbSet.Find(id);
            var entityList = new List<TEntity>
            {
                singleEntity
            };

            return entityList.AsQueryable();
        }

        /// <summary>Adds many entities.</summary>
        /// <param name="entities">The entity collection.</param>
        /// <returns>A value indicating whether the add was successful.</returns>
        public bool AddMany(IEnumerable<TEntity> entities)
        {
            try
            {
                this.dbSet.AddRange(entities);
                return true;
            }
            catch
            {
                // TODO log this error
                return false;
            }
        }

        /// <summary>Adds the entity.</summary>
        /// <param name="entity">The entity.</param>
        /// <returns>A value indicating whether the add was successful.</returns>
        public bool Add(TEntity entity)
        {
            try
            {
                this.dbSet.Add(entity);
                return true;
            }
            catch
            {
                // TODO log this error
                return false;
            }
        }

        /// <summary>Deletes the entity with the specified identifier.</summary>
        /// <param name="id">The identifier.</param>
        public void Delete(object id)
        {
            var entityToDelete = this.dbSet.Find(id);
            this.Delete(entityToDelete);
        }

        /// <summary>Deletes the specified entity.</summary>
        /// <param name="entity">The entity.</param>
        public void Delete(TEntity entity)
        {
            if (this.context.Entry(entity).State == EntityState.Detached)
            {
                this.dbSet.Attach(entity);
            }

            this.dbSet.Remove(entity);
        }

        /// <summary>
        ///  Updates the specified entity.
        /// </summary>
        /// <param name="entity">The entity</param>
        /// <param name="id">Primary key of the entity.</param>
        public void Update(TEntity entity, object id)
        {
            var originalObject = this.dbSet.Find(id);

            if (originalObject != entity)
            {
                this.context.Entry(originalObject).CurrentValues.SetValues(entity);
            }

            this.Save();
        }

        /// <summary>Saves this instance.</summary>
        /// <returns>A value indicating whether the save was successful.</returns>
        public bool Save()
        {
            try
            {
                return this.context.SaveChanges() > 0;
            }
            catch
            {
                // TODO log this error
                return false;
            }
        }
        #endregion
    }
}